const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken")
const config = require("../config/config").get(process.env.NODE_ENV);
const SALT_I = 10;

const userSchema = mongoose.Schema({
     email:{
         type:String,
         required:true,
         trim:true,
         unique:1
     },
     password:{
        type:String,
        required:true,
        minlength:6
     },
     name:{
         type:String,
         maxlength:30
     },
     lastname:{
         type:String,
         maxlength:20
     },
     role:{
         type:Number,
         default:0
     },
     token:{
         type:String
     }
})

userSchema.pre('save', function (next) {
    var user = this;

    if(user.isModified('password')){
        var salt = bcrypt.genSaltSync(SALT_I);
        var hash = bcrypt.hashSync(user.password, salt);
        user.password = hash;
        next();
    } else {
        next();
    }
})

userSchema.methods.comparePassword = function (candidatePassword, cb) {
    bcrypt.compareSync(candidatePassword, this.password, (err, isMatch) => {
        if(err) return cb(err);
       return  cb(null, isMatch);
    });
}

userSchema.methods.generateToken = function (cb) {
    var user = this;

    var token = jwt.sign(user._id.toHexString(), config.SECRET);

    user.token = token;
    user.save((err, user) => {
        if(err) cb(err);
        cb(null,user);
    })
}

const User = mongoose.model("User", userSchema);

module.exports = {User}