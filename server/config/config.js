const config = {
    production:{
        SECRET: process.env.SECRET,
        DATABASE: process.env.MONGODB_URI
    },
    default:{
        SECRET: "parol",
        DATABASE: "mongodb+srv://habibov:js1234@bookshelf.zini4.mongodb.net/books?retryWrites=true&w=majority"
    }
}

exports.get = function get(env) {
    return config[env] || config.default
}