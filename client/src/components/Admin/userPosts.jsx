import React from 'react';
import  { connect } from "react-redux";
import { getUserPosts } from "../../actions";
import moment from "moment-js";
import { Link } from 'react-router-dom'
import { useEffect } from 'react';


const UserPosts = (props) => {

    useEffect(() => {
        props.dispatch(getUserPosts(props.user.login.id))
    }, [])
    let user = props.user;

    const showUserPosts = (user) => (
        user.userPosts ?  
            user?.userPosts.map(post => (
                <tr key={post._id}>
                    <td><Link to={`/user/edit-post/${post._id}`}>{post.name}</Link></td>
                    <td>{post.author}</td>
                    <td>{moment(post?.createdAt).format('MM/DD/YY')}</td>
                </tr>
            ))
        :null
    )

    return (
        <div className='user_posts'>
            <h4>Your Reviews:</h4>
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Author</th>
                        <th>Date</th>

                    </tr>
                </thead>
                <tbody>
                    {showUserPosts(user)}
                </tbody>
            </table>
        </div>
    )
}

function mapStateToProps (state) {
    return {
        user: state.user,

    }
}

export default connect(mapStateToProps)(UserPosts)
