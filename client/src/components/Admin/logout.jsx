import React from 'react';
import axios from 'axios';
import { useNavigate } from "react-router-dom";

const Logout = (props) => {
    let navigate = useNavigate();
    let request = axios.get(`/logout`)
                        .then(res => {
                            setTimeout(()=> {
                                navigate("/")
                            },2000)
                        } );
  return (
    <div className='logout_container'>
            <h1>Sorry to see you go :(</h1>
    </div>);
};

export default Logout;
