import React from 'react'
import { Link } from 'react-router-dom'
import FontAwesome from "react-fontawesome";
import {connect} from "react-redux";    

const SideNavItems = ({user}) => {

    const items = [
        {
            type:"navItem",
            icon:'home',
            text:'Home',
            link:"/",
            restricted:false
        },
        {
            type:"navItem",
            icon:'file-text',
            text:'My Profile',
            link:"/user",
            restricted:true
        },
        {
            type:"navItem",
            icon:'users-cog',
            text:'Add Admins',
            link:"/user/register",
            restricted:true
        },
        {
            type:"navItem",
            icon:'file-pdf',
            text:'Add Reviews',
            link:"/user/add",
            restricted:true
        },
        {
            type:"navItem",
            icon:'file-pdf',
            text:'My Reviews',
            link:"/user/user-reviews",
            restricted:true
        },
        {
            type:"navItem",
            icon:'sign-in-alt',
            text:'Login',
            link:"/login",
            restricted:false,
            exlude:true
        },
        {
            type:"navItem",
            icon:'sign-out-alt',
            text:'Logout',
            link:"/user/logout",
            restricted:true
        },
    ]

    return (
        <div>
        {user.login ?
            items.map((item ,i) => {
                if(user.login.isAuth){
                   return !item.exlude ?
                    (<div key={i} className={item.type}>
                        <Link to ={item.link} >
                            <FontAwesome name={item.icon} /> {item.text}
                        </Link>
                    </div>) : null
                }else {
                    return !item.restricted ?
                    <div key={i} className={item.type}>
                        <Link to ={item.link} >
                            <FontAwesome name={item.icon} /> {item.text}
                        </Link>
                    </div>
                    :null
                }
            }) : null
        }  
        </div>
    )
}

function mapStateToProps(state){
    return{
        user:state.user
    }
}

export default connect(mapStateToProps)(SideNavItems)