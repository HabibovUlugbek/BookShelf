import React, { useState } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { addBook , clearNewBook } from "../../actions"

const AddReview = (props) => {
    const [formData, setFormData] = useState({
    name:'',
    author:"",
    review:"", 
    pages:"",
    rating:"",
    price:""
})
    const submitForm = (e) => {
        e.preventDefault()
        props.dispatch(addBook({
            ...formData,
            ownerId:props?.user.login.id
        }))
    }

    const showNewBook = (book) => (
        book.post ? 
            <div className='conf_link'>
                Cool !!! <Link to={`/books/${book.bookId}`} >Click to the link to see the post </Link>
            </div>
        :null
    )

    return (
        <div className='rl_container article'>
            <form onSubmit={submitForm}>
                <h2>Add Reivew</h2>


                <div className='form_element'>
                    <input 
                    type="text" 
                    placeholder='Enter name'
                    value={formData.name}
                    onChange={(e) => setFormData({...formData,name:e.target.value})} />
                </div>

                <div className='form_element'>
                    <input 
                    type="text" 
                    placeholder='Enter author'
                    value={formData.author}
                    onChange={(e) => setFormData({...formData,author:e.target.value})} />
                </div>

                <textarea  
                value={formData.review}
                onChange={(e) => setFormData({...formData,review:e.target.value})}
                />
                

                <div className='form_element'>
                    <input 
                    type="number" 
                    placeholder='Enter pages'
                    value={formData.pages}
                    onChange={(e) => setFormData({...formData,pages:e.target.value})} />
                </div>

                <div className='form_element'>
                   <select
                   value={formData.rating}
                   onChange={(e) => setFormData({...formData,rating:e.target.value})} 
                   >
                       <option val={1}>1</option>
                       <option val={2}>2</option>
                       <option val={3}>3</option>
                       <option val={4}>4</option>
                       <option val={5}>5</option>

                   </select>
                </div>

                <div className='form_element'>
                    <input 
                    type="number" 
                    placeholder='Enter price'
                    value={formData.price}
                    onChange={(e) => setFormData({...formData,price:e.target.value})} />
                </div>


                <button type="submit">Add review</button>

                {
                    props.books.newbook ? 
                        showNewBook(props.books.newbook)
                    :null
                }

            </form>
        </div>
    )
}

function mapStateToProps(state){
    console.log(state)
    return {
        books:state.books
    }
}

export default connect(mapStateToProps)(AddReview);
