import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { connect } from 'react-redux';
import { getUsers, userRegister } from '../../actions';


const Register = (props) => {
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
    const [lastname, setLastname] = useState('');
    const [email, setEmail] = useState('');
    const [error, setError] = useState('');



    let users = props.user.users;

    useEffect(() => {
      props.dispatch(getUsers())
    
    }, []);
    

    const showUsers = (users) => {
        return users ? 
            users.map(user => (
                <tr key={user._id}>
                    <td>{user.name}</td>
                    <td>{user.lastname}</td>
                    <td>{user.email}</td>
                </tr>
            ))
        : null;
    }
    const handleInputEmail= (e) => {
       setEmail(e.target.value)
    }
    const handleInputPassword = (e) => {
        setPassword(e.target.value)
    }
    const handleInputLastname= (e) => {
        setLastname(e.target.value)
    }
    const handleInputName= (e) => {
        setName(e.target.value)
    }

    const submitForm = (e) => {
        e.preventDefault();
        setError('')

        props.dispatch(userRegister(
            {
                email:email,
                password:password,
                name:name,
                lastname:lastname
            }, props.user.users
        ))
    }
console.log(props)

    useEffect(() => {
        if(props.user.register === false){
            setError("Error , try again")
            console.log("error");
        }else {
            console.log("ishladi");
            setPassword("")
            setEmail("")
            setName("")
            setLastname("")
        }
      
      }, [props]);
  return (
    <div className='rl_container'>
        <form onSubmit={submitForm}>
            <h2>Add User</h2>

            <div className="form_element">
                <input 
                type="text"
                placeholder='Enter name'
                value={name}
                onChange={handleInputName}
                 />
            </div>
            <div className="form_element">
                <input 
                type="text"
                placeholder='Enter lastname'
                value={lastname}
                onChange={handleInputLastname}
                 />
            </div>
            <div className="form_element">
                <input 
                type="email"
                placeholder='Enter email'
                value={email}
                onChange={handleInputEmail}
                 />
            </div>
            <div className="form_element">
                <input 
                type="password"
                placeholder='Enter password'
                value={password}
                onChange={handleInputPassword}
                 />
            </div>

            <button type='submit'>Add user</button>
            <div className='error'>
                {error}
            </div>
        </form>

        <div className="current_users">
            <h4>Current Users:</h4>
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Lastname</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        showUsers(users)
                    }
                </tbody>
            </table>
        </div>
    </div>);
};

function mapStateToProps(state){
    return{
        user:state.user
    }
}

export default connect(mapStateToProps)(Register);
