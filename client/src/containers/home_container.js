import React, { useEffect } from 'react'
import { connect } from 'react-redux';
import { getBooks } from '../actions';
import BookItem from '../widgets/book_item';
import login from './Admin/login';

const HomeContainer = (props) => {
    console.log(props)
    useEffect(() => {
        props.dispatch(getBooks(2,0,'desc'))
        
    }, [])

    const renderItems = (books) => (
        books?.list ? 
            books?.list?.map(book => (
                <BookItem {...book} key={book._id} />
            ))
        :null
    )

    const loadmore = () => {
        let count = props.books.list.length
        props.dispatch(getBooks(2,count,'desc',props.books.list))
    }
    return (
       
        <div>
            {renderItems(props.books)}
            <div 
            className="loadmore"
            onClick={loadmore}>
                Load More
            </div>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        books:state.books,
        user:state.user
    }
}

export default connect(mapStateToProps)(HomeContainer)
