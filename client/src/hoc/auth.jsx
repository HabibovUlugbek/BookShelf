import React , { useState} from "react";
import { useEffect } from "react";
import  {connect} from "react-redux"
import { auth } from '../actions'
import { useNavigate } from 'react-router-dom';

const AuthenticationCheck = (props) => {

    let navigate = useNavigate();
    const [loading, setLoading] = useState(false)
    useEffect(() => {
        props.dispatch(auth())
        setLoading(props.login?.isAuth)
        if(!loading){
            navigate("/login");
        }else {
            if(props.reload === false) {
                navigate("/user");
            }
        }
    }, [loading])


    const childrenWithProps = React.Children.map(props.children, child => {
        if (React.isValidElement(child)) {
          return React.cloneElement(child, {...props});
        }
        return child;
      });

    if(loading){
        return <div className="loader">Loading</div>
    }
    return (
        <div>
                {childrenWithProps}
        </div>
    )
}

function mapStateToProps(state){
        return { 
            user:state.user
        }
    }
export default connect(mapStateToProps)(AuthenticationCheck)
