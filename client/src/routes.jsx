import Home from "./components/Home/home"
import {Routes , Route}  from "react-router-dom"
import Layout from "./hoc/layout"
import BookView from "./components/Books"
import Login from "./containers/Admin/login"

import Auth from "./hoc/auth"
import User from "./components/Admin"
import AddReview from "./containers/Admin/add.jsx"
import EditReview from "./containers/Admin/edit.jsx"
import UserPosts from "./components/Admin/userPosts"
import Register from "./containers/Admin/register"
import Logout from "./components/Admin/logout"

const MainRoutes = () => {
    return (
       <Layout>
            <Routes>
                <Route path="/" exact  element={
                    <Auth >
                        <Home reload={null} />
                    </Auth>
                }/>
                <Route path="/books/:id" exact  element={
                    <Auth >
                        <BookView />
                    </Auth>
                }/>
                <Route path="/login" exact  element={
                    <Auth >
                        <Login reload={false} />
                    </Auth>
                }/>
                <Route path="/user" exact  element={
                    <Auth >
                        <User  reload={true}/>
                    </Auth>
                }/>
                <Route path="/user/logout" exact  element={
                    <Auth >
                        <Logout  reload={true}/>
                    </Auth>
                }/>
                 <Route path="/user/add" exact  element={
                    <Auth >
                        <AddReview reload={true}/>
                    </Auth>
                }/>
                <Route path="/user/register" exact  element={
                    <Auth >
                        <Register  reload={true}/>
                    </Auth>
                }/>
                 <Route path="/user/edit-post/:userId" exact  element={
                    <Auth >
                        <EditReview reload={true}/>
                    </Auth>
                }/>
                <Route path="/user/user-reviews" exact  element={
                    <Auth >
                        <UserPosts reload={null}/>
                    </Auth>
                }/>
            </Routes>
       </Layout>
    )
}

export default MainRoutes
